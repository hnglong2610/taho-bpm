1. Setup postgres database and create new database "camunda"
2. Look into the application.properties file for more information
3. 1st time running camunda.bpm.database.schema-update=create-drop. We should not use it in production mode. Use true instead of create-drop
4. Download https://camunda.com/download/modeler/ to design a flow