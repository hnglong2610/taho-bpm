package com.taho.b2b.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.extension.reactor.bus.CamundaSelector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@CamundaSelector(event = ExecutionListener.EVENTNAME_START)
@DependsOn("reactorPlugin")
public class MyEventCreateListener implements ExecutionListener {

	@Value("${taho-b2b.url}")
	private String tahoB2bServerUrl;
	
	@Value("${taho-b2c.url}")
	private String tahoB2cServerUrl;

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		execution.setVariable("tahoB2bServerUrl", tahoB2bServerUrl);
		execution.setVariable("tahoB2cServerUrl", tahoB2cServerUrl);
	}
}