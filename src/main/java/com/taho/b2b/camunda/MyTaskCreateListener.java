package com.taho.b2b.camunda;

import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.extension.reactor.bus.CamundaSelector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@CamundaSelector(event = TaskListener.EVENTNAME_CREATE)
@DependsOn("reactorPlugin")
public class MyTaskCreateListener implements TaskListener {

	@Value("${taho-b2b.url}")
	private String tahoB2bServerUrl;
	
	@Value("${taho-b2c.url}")
	private String tahoB2cServerUrl;

	@Override
	public void notify(DelegateTask delegateTask) {
		delegateTask.setVariable("tahoB2bServerUrl", tahoB2bServerUrl);
		delegateTask.setVariable("tahoB2cServerUrl", tahoB2cServerUrl);	
	}
}