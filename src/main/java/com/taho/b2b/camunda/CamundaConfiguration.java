package com.taho.b2b.camunda;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin;
import org.camunda.bpm.extension.reactor.CamundaReactor;
import org.camunda.bpm.spring.boot.starter.configuration.Ordering;
import org.camunda.connect.plugin.impl.ConnectProcessEnginePlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;

@Configuration
public class CamundaConfiguration {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Environment env;

	@Bean(name = "reactorPlugin")
	@Order(Ordering.DEFAULT_ORDER + 1)
	public ProcessEnginePlugin reactorPlugin() {
		return CamundaReactor.plugin();
	}

	@Bean(name = "connectPlugin")
	@Order(Ordering.DEFAULT_ORDER + 1)
	public ProcessEnginePlugin connectProcessPlugin() {
		ConnectProcessEnginePlugin plugin = new ConnectProcessEnginePlugin();
		return plugin;
	}

	@Bean
	@Primary
	@Profile("test")
	public DataSource getDataSource() throws Exception {
		BasicDataSource datasource = new BasicDataSource();
		log.info("data source url: " + env.getProperty("db1.datasource.url"));
		datasource.setDriverClassName(env.getProperty("db1.datasource.driverClassName"));
		datasource.setUrl(env.getProperty("db1.datasource.url"));
		datasource.setUsername(env.getProperty("db1.datasource.username"));
		datasource.setPassword(env.getProperty("db1.datasource.password"));
		datasource.setMaxActive(Integer.valueOf(env.getProperty("spring.datasource.maxActive", "5")));
		return datasource;

	}

//	@Bean("newDs")
//	@Profile("test")
//	public DataSource getNewDataSource() throws Exception {
//		BasicDataSource datasource = new BasicDataSource();
//		datasource.setDriverClassName(env.getProperty("db2.datasource.driverClassName"));
//		datasource.setUrl(env.getProperty("db2.datasource.url"));
//		datasource.setUsername(env.getProperty("db2.datasource.username"));
//		datasource.setPassword(env.getProperty("db2.datasource.password"));
//		datasource.setMaxActive(Integer.valueOf(env.getProperty("spring.datasource.maxActive", "5")));
//		return datasource;
//
//	}

	@Value("${spring.jersey.application-path}")
	private String contextPath;
}