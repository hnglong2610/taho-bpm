package com.taho.b2b.camunda;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;

import org.camunda.bpm.engine.AuthorizationException;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.ProcessEngineException;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.exception.NotFoundException;
import org.camunda.bpm.engine.exception.NotValidException;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.impl.persistence.entity.ActivityInstanceImpl;
import org.camunda.bpm.engine.impl.util.IoUtil;
import org.camunda.bpm.engine.repository.DecisionDefinition;
import org.camunda.bpm.engine.repository.DecisionDefinitionQuery;
import org.camunda.bpm.engine.rest.dto.repository.DecisionDefinitionDiagramDto;
import org.camunda.bpm.engine.rest.dto.repository.DecisionDefinitionDto;
import org.camunda.bpm.engine.rest.dto.repository.DecisionDefinitionQueryDto;
import org.camunda.bpm.engine.rest.dto.runtime.ActivityInstanceDto;
import org.camunda.bpm.engine.rest.dto.runtime.TransitionInstanceDto;
import org.camunda.bpm.engine.rest.exception.InvalidRequestException;
import org.camunda.bpm.engine.rest.exception.RestException;
import org.camunda.bpm.engine.rest.impl.AbstractRestProcessEngineAware;
import org.camunda.bpm.engine.rest.mapper.JacksonConfigurator;
import org.camunda.bpm.engine.rest.spi.impl.AbstractProcessEngineAware;
import org.camunda.bpm.engine.rest.util.EngineUtil;
import org.camunda.bpm.engine.runtime.ActivityInstance;
import org.camunda.bpm.engine.runtime.TransitionInstance;
import org.camunda.bpm.spring.boot.starter.rest.CamundaJerseyResourceConfig;
import org.glassfish.jersey.internal.util.collection.ImmutableMultivaluedMap;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Override the rest configuration of camunda rest api Customize rest service
 * for the need of Summit application
 * 
 * @author hxha
 *
 */

@Service
@ApplicationPath("/rest")
public class TahoRestApplication extends CamundaJerseyResourceConfig {

	@Override
	protected void registerAdditionalResources() {
		Set<Class<?>> classes = new HashSet<Class<?>>();

		classes.add(CustomVariableInstanceRest.class);
		this.registerClasses(classes);
	}

	@Path("/custom")
	public static class CustomVariableInstanceRest extends AbstractRestProcessEngineAware {
		
		public CustomVariableInstanceRest() {			
			super(null,null);			
		}
		
		public CustomVariableInstanceRest(String engineName, ObjectMapper objectMapper) {
			super(engineName, objectMapper);
		}
		
	



		@GET
		@Path("/decision-definition/{name}/xml")
		@Produces(MediaType.APPLICATION_JSON)
		public DecisionDefinitionDiagramDto getLastestDecisionDefinitionXML(@PathParam("name") String name) {
			Map<String,String> hashMap = new HashMap<>();
			hashMap.put("sortBy", "version");
			hashMap.put("sortOrder", "desc");
			hashMap.put("name", name);
			MultivaluedMap<String, String> map = new MultivaluedHashMap<>(hashMap) ;
			DecisionDefinitionQueryDto queryDto = new DecisionDefinitionQueryDto(getObjectMapper(), map );
			DecisionDefinitionQuery query = queryDto.toQuery(getProcessEngine());
			
			List<DecisionDefinition> listPage = query.listPage(0, 1);
			DecisionDefinition decisionDefinition = listPage.get(0);
			
			return getDecisionDefinitionDmnXml(decisionDefinition.getId()); 
		}
		
		
		  private DecisionDefinitionDiagramDto getDecisionDefinitionDmnXml(String decisionDefinitionId) {
			    InputStream decisionModelInputStream = null;
			    try {
			      decisionModelInputStream = getProcessEngine().getRepositoryService().getDecisionModel(decisionDefinitionId);

			      byte[] decisionModel = IoUtil.readInputStream(decisionModelInputStream, "decisionModelDmnXml");
			      return DecisionDefinitionDiagramDto.create(decisionDefinitionId, new String(decisionModel, "UTF-8"));

			    } catch (NotFoundException e) {
			      throw new InvalidRequestException(Status.NOT_FOUND, e, e.getMessage());

			    } catch (NotValidException e) {
			      throw new InvalidRequestException(Status.BAD_REQUEST, e, e.getMessage());

			    } catch (ProcessEngineException e) {
			      throw new RestException(Status.INTERNAL_SERVER_ERROR, e);

			    } catch (UnsupportedEncodingException e) {
			      throw new RestException(Status.INTERNAL_SERVER_ERROR, e);

			    } finally {
			      IoUtil.closeSilently(decisionModelInputStream);
			    }
			  }
		
		

		@GET
		@Path("/{processInstanceId}/activity-instances")
		@Produces(MediaType.APPLICATION_JSON)
		public ActivityInstanceDto getLastestChildActivityInstance(
				@PathParam("processInstanceId") String processInstanceId) {

			RuntimeService runtimeService = processEngine.getRuntimeService();

			ActivityInstance activityInstance = null;

			try {
				activityInstance = runtimeService.getActivityInstance(processInstanceId);
			} catch (AuthorizationException e) {
				throw e;
			} catch (ProcessEngineException e) {
				throw new InvalidRequestException(Status.INTERNAL_SERVER_ERROR, e, e.getMessage());
			}

			if (activityInstance == null) {

				HistoryService historyService = processEngine.getHistoryService();
				HistoricProcessInstance instance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(processInstanceId).singleResult();
				if (instance != null)
				{
					ActivityInstanceImpl actInstance = new ActivityInstanceImpl();
					actInstance.setActivityId(instance.getEndActivityId());
					actInstance.setActivityName(instance.getProcessDefinitionName());
					actInstance.setProcessDefinitionId(instance.getProcessDefinitionId());
					actInstance.setProcessInstanceId(instance.getId());
					ActivityInstanceDto result = ActivityInstanceDto.fromActivityInstance(actInstance);
					return result;
				}
			}
			ActivityInstance latestActivityInstance = getLatestActivityInstance(activityInstance);

			if (latestActivityInstance != null) {
				return ActivityInstanceDto.fromActivityInstance(latestActivityInstance);
			} else {
				TransitionInstance latestTransitionInstance = getLatestTransitionInstance(activityInstance);
				ActivityInstanceImpl instance = new ActivityInstanceImpl();
				instance.setActivityId(latestTransitionInstance.getActivityId());
				instance.setActivityName(latestTransitionInstance.getActivityName());
				instance.setProcessDefinitionId(latestTransitionInstance.getProcessDefinitionId());
				instance.setProcessInstanceId(latestTransitionInstance.getProcessInstanceId());
				ActivityInstanceDto result = ActivityInstanceDto.fromActivityInstance(instance);
				return result;
			}

		}

		private ActivityInstance getLatestActivityInstance(ActivityInstance activityInstance) {

			ActivityInstance[] childActivityInstances = activityInstance.getChildActivityInstances();

			if (childActivityInstances == null || childActivityInstances.length == 0) {
				return activityInstance;
			}

			return getLatestActivityInstance(childActivityInstances[0]);

		}

		private TransitionInstance getLatestTransitionInstance(ActivityInstance activityInstance) {

			TransitionInstance[] childTransitionInstances = activityInstance.getChildTransitionInstances();

			if (childTransitionInstances != null && childTransitionInstances.length > 0) {

				return childTransitionInstances[0];
			}

			ActivityInstance[] childActivityInstances = activityInstance.getChildActivityInstances();

			if (childActivityInstances[0].getChildActivityInstances() != null
					&& childActivityInstances[0].getChildActivityInstances().length > 0) {
				return getLatestTransitionInstance(childActivityInstances[0]);
			}
			return null;

		}
	}

}